module type S = sig
  val s: Cursor.t -> char -> bool
  val e: Cursor.t -> char -> bool
end

(*let str c = String.make 1 c*)

let newline = function '\n' -> true | _ -> false
let printable ch = ch >= ' ' && ch <= '~'
let letter ch = (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')
let digit = function '0' .. '9' -> true | _ -> false
