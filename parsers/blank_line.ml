module type Fn = sig
  type t
  val blank_line: t -> t
end

module Make (F : Fn) = struct
  type t = F.t
  let s _cur = function '\n' -> true | _ -> false
  let e _cur = function '\n' -> true | _ -> false
  let parse _cursor acc = F.blank_line acc
end
