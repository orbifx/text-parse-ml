module type Fn = sig
  type t
  val key_value: string -> string -> t -> t
end

open Text_parse.Syntax
open Text_parse.Cursor

module Make (F : Fn) = struct
  type t = F.t
  let s _cur c = letter c
  let e _cur c = newline c
  let parse cursor acc =
    let colon_pos = match find_end (fun _cur c -> c = ':') cursor with
        Some x -> x - cursor.pos - 1 | None -> 0 in (*todo:None shouldn't be allowed by scope*)
    let key = segment_string { cursor with right_boundary = cursor.pos+colon_pos } in
    let value = segment_string { cursor with pos = cursor.pos+colon_pos+1; right_boundary = cursor.right_boundary } in
    F.key_value key value acc
end
