module type Fn = sig
  type t
  val heading_hashbang: int -> string -> t -> t
end

open Text_parse.Syntax
open Text_parse.Cursor

module Hashbang (F : Fn) = struct
  type t = F.t
  let s _cur = function '#' -> true | _ -> false
  let e _cur = newline
  let parse cursor acc =
    let level = match find_end (fun _cur c -> c <> '#') cursor with
        Some x -> x - cursor.pos - 1 | None -> 0 in
    F.heading_hashbang level (segment_string { cursor with pos = cursor.pos + level + 1; right_boundary = cursor.right_boundary-1 }) acc
end
