module type Fn = sig
  type t
  val bullet_item_s: char -> 'a -> 'a
  val bullet_item_e: 'a -> 'a
  val bullet_list_s: 'a -> 'a
  val bullet_list_e: 'a -> 'a
end

open Text_parse.Parser
open Text_parse.Syntax
open Text_parse.Cursor

module Item (F : Fn) = struct
  type t = F.t
  let s _cursor = function '-' | '+' | '*' -> true | _ -> false
  let e cursor _ch = newline (char_at cursor 1)
  let subsyntaxes = [||]
  let parse cur acc =
    let bullet_char = char cur in
    let left = match find_end (fun _cur c -> c <> ' ') { cur with pos = cur.pos + 1 }
      with Some x -> x-1 | None -> 0 in
    F.bullet_item_s bullet_char acc |> parse subsyntaxes (sub ~left cur) |> F.bullet_item_e
end

module List (F : Fn) = struct
  type t = F.t
  let s _cursor = function '-' | '+' | '*' -> true | _ -> false
  let e cursor _ch = newline (char_at cursor 1) && not (s cursor (char_at cursor 2))
  let subsyntaxes = [| (module Item (F) : Text_parse.Parser.S with type t = F.t) |]
  let parse cur acc = F.bullet_list_s acc |> parse subsyntaxes cur |> F.bullet_list_e
end
