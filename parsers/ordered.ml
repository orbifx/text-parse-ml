module type Fn = sig
  type t
  val ordered_item_s: char -> 'a -> 'a
  val ordered_item_e: 'a -> 'a
  val ordered_list_s: 'a -> 'a
  val ordered_list_e: 'a -> 'a
end

open Text_parse.Parser
open Text_parse.Syntax
open Text_parse.Cursor

module Item (F : Fn) = struct
  type t = F.t
  let s cur ch =
    let is_enum c = (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') in
    let is_delim c = c = '.' || c = ')' in
    is_enum ch && is_delim (char_at cur 1)
  let e cursor _ch = newline (char_at cursor 1)
  let subsyntaxes = [||]
  let parse cur acc =
    let bullet_char = char cur in
    let left = match find_end (fun _cur c -> c <> ' ') { cur with pos = cur.pos + 1 }
      with Some x -> x | None -> 0 in
    F.ordered_item_s bullet_char acc |> parse subsyntaxes (sub ~left cur) |> F.ordered_item_e
end

module List (F: Fn) = struct
  type t = F.t
  let s cur ch =
    let is_enum c = (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') in
    let is_delim c = c = '.' || c = ')' in (*todo: can't have `.` if sentence ends with it and lists are in sense*)
    is_enum ch && is_delim (char_at cur 1)
  let e cursor _ch = newline (char_at cursor 1) && newline (char_at cursor 2)(* not (s {cursor with pos = cursor.pos+2} (char_at cursor 2)) *)
  let subsyntaxes = [| (module Item (F) : Text_parse.Parser.S with type t = F.t) |]
  let parse cur acc = F.ordered_list_s acc |> parse subsyntaxes cur |> F.ordered_list_e
end
