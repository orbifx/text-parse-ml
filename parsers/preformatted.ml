module type Fn = sig
  type t
  val tab_preformatted: string -> t -> t
end

open Text_parse.Cursor

module Tabbed (F : Fn) = struct
  type t = F.t
  let s _cur ch = '\t' = ch
  let e cur = function '\n' -> not (char_at cur 1 = '\t') | _ -> false
  let parse cur acc = F.tab_preformatted (segment_string cur) acc
end
