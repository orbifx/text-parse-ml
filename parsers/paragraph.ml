module type Fn = sig
  type t
  val paragraph_s: t -> t
  val paragraph_e: t -> t
end

open Text_parse.Parser
open Text_parse.Syntax
open Text_parse.Cursor

module Make (F : Fn)(S : Text_parse.Parser.Sub_parsers with type t = F.t) = struct
  type t = F.t
  let s _cur ch = printable ch
  let e cur = function
    | '\n' -> char_at cur (-1) = '\n'
    | _ when cur.pos + 1 = cur.right_boundary -> true
    | _ -> false
  let parse cur acc = F.paragraph_s acc |> parse S.subparsers cur |> F.paragraph_e
end
