module type Fn = sig
  val bold: string -> 'a -> 'a
  val italic: string -> 'a -> 'a
  val underline: string -> 'a -> 'a
  val inline_monospace: string -> 'a -> 'a
end

open Text_parse.Cursor

module Bold (F : Fn) = struct
  let s _cursor = function '*' -> true | _ -> false
  let e = s
  let parse cur acc = F.bold (segment_string (unwrap 1 cur)) acc
end

module Italic (F : Fn) = struct
  let s _cursor = function '/' -> true | _ -> false
  let e = s
  let parse cur acc = F.italic (segment_string (unwrap 1 cur)) acc
end

module Underline (F : Fn) = struct
  let s _cursor = function '_' -> true | _ -> false
  let e = s
  let parse cur acc = F.underline (segment_string (unwrap 1 cur)) acc
end

module Inline_monospace (F : Fn) = struct
  let s _cursor = function '`' -> true | _ -> false
  let e = s
  let parse cur acc = F.inline_monospace (segment_string (unwrap 1 cur)) acc
end
