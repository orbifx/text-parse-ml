module type Markdown_t = sig
  include Blank_line.Fn
  include Reference.Fn with type t := t
  include Bullet.Fn with type t := t
  include Ordered.Fn with type t := t
  include Heading.Fn with type t := t
  include Preformatted.Fn with type t := t
  include Paragraph.Fn with type t := t
end

open Text_parse.Parser
open Text_parse.Cursor

module Make (F : Markdown_t) = struct
  let subsyntaxes = [|
      (module Blank_line.Make (F) : Text_parse.Parser.S with type t = F.t);
      (module Heading.Hashbang (F));
      (module Reference.Referent (F));
      (module Bullet.List (F));
      (module Ordered.List (F));
      (module Preformatted.Tabbed (F));
      (*(module Paragraph.Make (F));*)
    |]

  let of_string text acc =
    parse subsyntaxes { text; pos = 0; right_boundary = String.length text - 1 } acc
end
