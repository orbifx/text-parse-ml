module type Fn = sig
  type t
  val plain_text: string -> t -> t
end

open Text_parse.Parser
open Text_parse.Cursor

module Plain_text (F : Fn) = struct
  type t = F.t
  let s _cursor _ch = true
  let e cursor = function
    | '\n' -> char_at cursor (-1) = '\n'
    | _ when cursor.pos + 1 = cursor.right_boundary -> true
    | _ -> false
  let parse cur acc = F.plain_text (segment_string cur) acc
end

module type Plain_text_t = sig
  include Blank_line.Fn
  include Heading.Fn with type t := t
  include Uri.Fn with type t := t
  include Paragraph.Fn with type t := t
  include Fn with type t := t
end


module Make (F : Plain_text_t) = struct
  module P = struct
    type t = F.t
    let subparsers = [| (module Plain_text (F) : Text_parse.Parser.S with type t = F.t); (module Uri.Angled (F)) |]
  end

  let subparsers = [| (module Paragraph.Make (F)(P) : Text_parse.Parser.S with type t = F.t); (module Blank_line.Make (F)); (module Heading.Hashbang (F)); (module Paragraph.Make (F)(P)); |]
  let of_string text acc = parse subparsers { text; pos = 0; right_boundary = String.length text - 1 } acc
end
