module type Fn = sig
  type t
  val angled_uri: string -> t -> t
end

open Text_parse.Syntax
open Text_parse.Cursor

module Angled (F : Fn) = struct
  type t = F.t
  let s cur = function '<' -> let c = char_at cur 1 in letter c || digit c | _ -> false
  let e _cur = function '>' -> true | _ -> false
  let parse cur acc = F.angled_uri (segment_string (unwrap 1 cur)) acc
end

(* module Uri (F : TextFn) = struct
 *   type t = F.t
 *   let rec is_scheme cur = function
 *     | ':' -> true
 *     | ch when letter ch -> is_scheme (next_char cur) (char_at cur 1)
 *     | _ -> false
 *   let s cur ch = letter ch && is_scheme (next_char cur) (char_at cur 1)
 *   let e cur _ch = match char_at cur 1 with '\n' | ' ' -> true | _ -> false
 *   let at = at s e
 *   let parse cur acc = F.angled_uri (segment_string cur) acc
 * end *)

