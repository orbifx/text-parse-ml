module Test = struct
  type t = unit
  let blank_line () = print_string "{bl}"
  let angled_uri s () = print_string ("{>}" ^ s ^ "{<}")
  let plain_text s () = print_string s
  let heading_hashbang i s () = print_string (string_of_int i ^ s)
  let paragraph_s () = print_string "{p>}"
  let paragraph_e () = print_string "{<p}"
  let key_value a b () = print_endline (a ^"~"^ String.trim b)
end

let () =
  let string_of_file filename =
    let ch = open_in filename in
    let s = really_input_string ch (in_channel_length ch) in
    close_in ch;
    s in
  let filename = Sys.argv.(1) in
  (* let module Parse = Text.MakeSimple (Html) in *)
  let module Parse = Parsers.Plain_text.Make (Test) in
  (*let subsyntaxes = [| (module Parser.Key_value.Make (Test) : Text.Parser with type t = Test.t) |] in*)
  (*let of_string text acc = Text.parse subsyntaxes { text; pos = 0; right_boundary = String.length text - 1 } acc in*)
  Parse.of_string (string_of_file filename) ()
