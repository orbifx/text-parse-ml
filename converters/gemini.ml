type t = string
let blank_line a = a ^ "\n"
let plain_text s a = a ^ s
let sentence_s a = a ^ ""
let sentence_e a = a ^ " "
let sentence_segment s a = a ^ " " ^ s
let reference_name n a = a ^ "[" ^ n ^ "]"
let bracketed_referent_s n a = a ^ "[" ^ n ^ "]: "
let bracketed_referent_e a = a ^ "\n"
let angled_uri u a = a ^ "\n=> " ^ u
let bold t a = a ^ "*" ^ t ^ "*"
let italic t a = a ^ "/" ^ t ^ "/"
let underline t a = a ^ "_" ^ t ^ "_"
let inline_monospace t a = a ^ "`" ^ t ^ "`"
let heading_hashbang lvl h a = a ^ String.make lvl '#' ^ h ^ "\n"
let paragraph_s a = a
let paragraph_e a = a
let preformatted s a = a ^ "<pre>" ^ s ^ "</pre>"
let bullet_list_s a = a
let bullet_list_e a = a
let bullet_item_s ch a = a ^ Char.escaped ch
let bullet_item_e a = a ^ "\n"
let ordered_list_s a = a
let ordered_list_e a = a
let ordered_item_s = bullet_item_s
let ordered_item_e = bullet_item_e
let key_value_pair k v a = prerr_endline @@ k ^ "~" ^ v; a

