let esc x =
  let fn a c = match c with
  | '&' -> a ^ "&amp;"
  | '<' -> a ^ "&lt;"
  | '"' -> a ^ "&quot;"
  | '\''-> a ^ "&apos;"
  | x -> a ^ String.make 1 x
  in
  Seq.fold_left fn "" (String.to_seq x)

type t = string
let blank_line a = a ^ ""
let plain_text s a = a ^ esc s
let sentence_s a = a ^ ""
let sentence_e a = a ^ " "
let sentence_segment s a = a ^ esc s ^ " "
let reference_name n a = a ^ {|<a href="#|} ^ n ^ {|">|} ^ esc n ^ "</a>"
let bracketed_referent_s n a = a ^ {|<a id="|} ^ n ^ {|">|} ^ esc n ^ "</a>: "
let bracketed_referent_e a = a ^ "<br/>"
let angled_uri u a = a ^ {|&lt;<a href="|} ^ u ^ {|">|} ^ esc u ^ {|</a>&gt;|}
let bold t a = a ^ "<b>" ^ esc t ^ "</b>"
let italic t a = a ^ "<i>" ^ esc t ^ "</i>"
let underline t a = a ^ "<u>" ^ esc t ^ "</u>"
let inline_monospace t a = a ^ "<code>" ^ esc t ^ "</code>"
let heading_hashbang lvl h a =
  let lvl = string_of_int lvl in
  a ^ "<h" ^ lvl ^ " id=\"" ^ esc (String.lowercase_ascii h) ^"\">" ^ esc h ^ "</h" ^ lvl ^ ">"
let paragraph_s a = a ^ "<p>"
let paragraph_e a = a ^ "</p>"
let preformatted s a = a ^ "<pre>" ^ esc s ^ "</pre>"
let bullet_list_s a = a ^ "<ul>"
let bullet_list_e a = a ^ "</ul>"
let bullet_item_s _ch a = a ^ "<li>"
let bullet_item_e a = a ^ "</li>"
let ordered_list_s a = a ^ "<ol>"
let ordered_list_e a = a ^ "</ol>"
let ordered_item_s = bullet_item_s
let ordered_item_e = bullet_item_e
let key_value k v a = prerr_endline @@ k ^ "~" ^ v; a
